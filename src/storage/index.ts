export class Storage<T = unknown> {
  protected namespace?: string;
  protected key?: string;

  constructor(options?: { key?: string; namespace?: string }) {
    this.key = options?.key;
    this.namespace = options?.namespace;
  }

  /**
   * 获取命名空间的key
   * @param key key
   */
  private getNamespaceKey(key: string = this.key!) {
    if (!this.namespace) {
      return key;
    }
    return `${this.namespace}:${key}`;
  }

  set(value: T): void;
  set(key: string, value: T): void;
  /**
   * 设置值，如果只传一个参数，那么key就是默认的key, 传入的参数就是value
   * @param key 键
   * @param value 值
   */
  set(key: string | T, value?: T) {
    // 参数归一化
    if (arguments.length === 1) {
      if (!this.key) {
        throw new Error(
          '没有设置key, 并且set方法只传了一个参数。 the key is not set, and the set method only passed one parameter.',
        );
      }
      value = key as T;
      key = this.key;
    }

    // 先获取命名空间的key
    key = this.getNamespaceKey(key as string);
    // 如果不是字符串，先转换为字符串
    if (typeof value !== 'string') {
      value = JSON.stringify(value) as T;
    }
    window.localStorage.setItem(key, value as string);
  }

  get(key?: string): T {
    if (arguments.length === 0) {
      if (!this.key) {
        throw new Error(
          '没有设置key, 并且get方法没有传入key。 the key is not set, and the get method does not pass the key.',
        );
      }
      key = this.key;
    }

    // 先获取命名空间的key
    key = this.getNamespaceKey(key);

    const value = window.localStorage.getItem(key);
    try {
      return JSON.parse(value!);
    } catch (e) {
      return value as T;
    }
  }

  remove(key: string) {
    window.localStorage.removeItem(key);
  }

  clear(): number {
    let count = 0;
    // 如果有命名空间，只清除命名空间的
    if (this.namespace) {
      const keys = Object.keys(window.localStorage);
      keys.forEach((key) => {
        if (key.startsWith(this.namespace!)) {
          window.localStorage.removeItem(key);
          count++;
        }
      });
    } else {
      console.warn('没有命名空间，清除全部localStorage');
      count = window.localStorage.length;
      window.localStorage.clear();
    }
    return count;
  }
}
