import { isNumber } from '../is';

declare global {
  interface String {
    toNumber(): number;
    /**
     * 转换为数字
     */
    toNumber(radix: number): number;
    toNumber(options: Partial<typeof defaultToNumberOptions>): number;
  }
}

export function installString() {
  Object.defineProperty(String.prototype, 'toNumber', {
    value: function (options: number | Partial<typeof defaultToNumberOptions> = defaultToNumberOptions) {
      return toNumber(this, options);
    },
    configurable: true,
    writable: true,
    enumerable: false,
  })
}

const defaultToNumberOptions = {
  /**
   * 进制
   */
  radix: 10,
  /**
   * @description 有无符号
   */
  signed: false,
}
/**
 *
 * @param str 被转换的字符串
 * @param options
 * @returns 转换后的数字
 */
export function toNumber(str: string, options: Partial<typeof defaultToNumberOptions> | number = defaultToNumberOptions) {
  if (isNumber(options)) {
    options = { radix: options };
  }

  const mixOptions = { ...defaultToNumberOptions, ...options };

  if (str.includes(".")) {
    return Number(str);
  }

  if (mixOptions.signed) {
    const num = parseInt(str, mixOptions.radix)
    const max = Math.pow(options.radix || 10, str.length) - 1;
    const binStrLen = max.toString(2).length;
    const binStr = num.toString(2);
    if (binStr.length !== binStrLen || binStr[0] === "0") {
      return num;
    } else if (binStr[0] === "1") {
      const binArr = binStr.split("");
      //首位取0
      binArr[0] = "0";
      //反码
      binArr.forEach((v, i) => {
        if (i === 0) return;
        if (v === "1") {
          binArr[i] = "0";
        } else if (v === "0") {
          binArr[i] = "1";
        }
      })
      //补码
      return -(parseInt(binArr.join(""), 2) + 1);
    }
  } else {
    return parseInt(str, mixOptions.radix);
  }
}

installString();
