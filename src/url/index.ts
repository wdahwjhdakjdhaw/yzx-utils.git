import { isArray, isNullObject, isObject, isString } from '../is';

/**
 * 解析url参数，返回对象，无参数返回空对象
 * @param url
 * @return {}
 */
export function parseUrlParams(url: string): Record<string, string> {
  const search = url.split('?')[1];
  if (!search) {
    return {};
  }
  return search.split('&').reduce((params: Record<string, string>, param) => {
    const [key, value] = param.split('=');
    params[key] = decodeURI(value);
    return params;
  }, {});
}

export function joinUrlParams(url: string, params: Record<string, unknown>) {
  let result = url;
  if (result.indexOf('?') === -1 && !isNullObject(params)) {
    result += '?';
  } else {
    result += '&';
  }

  for (const key in params) {
    if (params.hasOwnProperty(key)) {
      if (isObject(params[key]) || isArray(params[key])) {
        params[key] = JSON.stringify(params[key]);
      }
      result += `${key}=${encodeURI(params[key] as string)}&`;
    }
  }

  if (result.charAt(result.length - 1) === '&') {
    result = result.substring(0, result.length - 1);
  }
  return result;
}

/**
 * 合并url, 最后一个参数如果传递对象，则作为query参数处理
 * @param args url参数
 * @return 合并后的url
 */
export function joinUrl(...args: (string | string[] | object)[]) {


  let queryParam: Record<string, unknown> = {};
  //如果最后一个是对象，则当作param处理
  if (isObject(args.last)) {
    queryParam = args.pop() as unknown as Record<string, unknown>;
  }

  const formatArgs: (string[] | string)[] = args as (string[] | string)[];

  const urls: string[] = [];
  const preUrls = [".", "..", "../"]
  const spilt = "/";
  for (let i = 0; i < args.length; i++) {
    let paths: string[] = args[i] as string[];
    if (isString(paths)) {
      paths = paths.split(spilt);
    }

    //如果是第一个元素则直接加入
    if (i === 0) {
      //如果最后一个是空字符串，则删掉
      if (paths.last === "") {
        paths.pop();
      }
      urls.push(...paths);
      continue;
    }


    if (paths.length) {
      for (const path of paths) {
        if (preUrls.includes(path)) {
          urls.pop();
          continue;
        }

        if (path) {
          urls.push(path);
        }
      }
    }
  }

  if ((isString(formatArgs.last) && formatArgs.last.endsWith(spilt) && !preUrls.includes(formatArgs.last)) || (isArray(formatArgs.last) && formatArgs.last.length && formatArgs.last?.last?.endsWith(spilt) && !preUrls.includes(formatArgs.last?.last))) {
    urls.push("");
  }

  if (queryParam) {
    return joinUrlParams(urls.join(spilt), queryParam as Record<string, unknown>);
  }

  return urls.join(spilt);
}
