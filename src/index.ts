export * from "./date/index";
export * from "./fun/index";
export * from "./is/index";
export * from "./storage/index";
export * from "./math/index";
export * from "./url/index";
export * from "./json/index"
export * from "./array/index";
export * from "./string/index";
export * from "./object/index";
export * from "./language/index";
export * from "./type/index";

import * as _Date from './date/index';
import * as _Fun from './fun/index';
import * as _Is from './is/index';
import * as _Storage from './storage/index';
import * as _Math from "./math/index";
import * as _Url from "./url/index";
import * as _Json from "./json/index"
import * as _Array from "./array/index"
import * as _String from "./string/index"
import * as _Object from "./object/index"
import * as _Language from "./language/index"

export const YDate = _Date;
export const YFun = _Fun;
export const YIs = _Is;
export const YStorage = _Storage;
export const YMath = _Math;
export const YUrl = _Url;
export const YJson = _Json;
export const YArray = _Array;
export const YString = _String;
export const YObject = _Object;
export const YLanguage = _Language;
export const YI18n = _Language.i18n;

const Y = {
  ..._Date,
  ..._Fun,
  ..._Is,
  ...Math,
  ..._Storage,
  ..._Url,
  ..._Json,
  ..._Array,
  ..._String,
  ..._Object
}

export default Y

