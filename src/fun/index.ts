import { isFunction } from '../is';
import { stringify } from '../json';

/**
 * 节流, 如果调用成功返回原函数执行结果，否则返回失败值，默认为false
 * @param fn 原函数
 * @param delay 延迟时间
 * @param failReturn 失败返回值， 默认为false
 */
export function throttle<T extends (...args: unknown[]) => unknown>(fn: T, delay: number, failReturn = false): T {
  let last = 0;
  return function (this: ThisType<T>, ...args: unknown[]) {
    const now = Date.now();
    if (now - last > delay) {
      const result = fn.apply(this, args);
      last = now;
      return result;
    }
    return failReturn;
  } as T;
}

/**
 * 防抖
 * @param fn 原函数
 * @param delay 延迟时间
 */
export function debounce<T extends (...args: unknown[]) => unknown>(fn: T, delay: number): T {
  let timer: NodeJS.Timeout | null = null;
  return function (this: ThisType<T>, ...args: unknown[]) {
    if (timer) {
      clearTimeout(timer);
    }
    timer = setTimeout(() => {
      fn.apply(this, args);
    }, delay);
  } as T;
}

export function cloneDeep<T>(object: T): T {
  function _deepClone<T>(obj: T, map = new WeakMap()) {
    if (typeof obj !== 'object' || obj === null || obj instanceof Function) {
      return obj;
    }
    if (map.has(obj)) {
      return map.get(obj);
    }
    //如果是对象类型，就创建一个新对象
    if (typeof obj === 'object') {
      let result: Record<string, unknown> | unknown[];
      if (Array.isArray(obj)) {
        result = [];
        map.set(obj, result);
        for (const item of obj) {
          result.push(_deepClone(item, map));
        }
      } else {
        result = {};
        map.set(obj, result);
        for (const key in obj) {
          if (obj.hasOwnProperty(key)) {
            result[key] = _deepClone(obj[key], map);
          }
        }
      }
      map.set(obj, result);

      return result;
    }
    //如果不是对象类型，直接返回
    return obj;
  }

  return _deepClone(object);
}

/**
 * 记忆函数
 */
export function memorize<T extends (...args: unknown[]) => R, R>(fn: T): (...args: Parameters<T>) => R {
  const params: Map<string, unknown>[] = [];
  if (!isFunction(fn)) {
    throw Error("memorize函数第一个参数必须为函数")
  }

  return  function(...args: Parameters<T>) {
    // 根据参数长度来进一步加快查询速度，因为长度不一致参数一定不一致
   const paramMap = params[args.length] || (params[args.length] = new Map());
   // 将参数数组字符串化
   const paramStr = stringify(args);

   if (paramMap.has(paramStr)) {
     return paramMap.get(paramStr) as R;
   }

   const result = fn(...args);
   paramMap.set(paramStr, result);
   return result;
  }
}


