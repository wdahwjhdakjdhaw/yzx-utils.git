type Join<K, P> = K extends string | number
  ? P extends string | number
    ? `${P}${P extends '' ? '' : '.'}${K}`
    : never
  : never;

type GetPaths<T, P = ''> = {
  [K in keyof T]: T[K] extends object
    ? GetPaths1<T[K], Join<K, P>>
    : Join<K, P>;
}[keyof T] | (P extends '' ? never : P);

type GetPaths1<T, P = ''> = {
  [K in keyof T]: T[K] extends object
    ? GetPaths2<T[K], Join<K, P>>
    : Join<K, P>;
}[keyof T] | (P extends '' ? never : P);

type GetPaths2<T, P = ''> = {
  [K in keyof T]: T[K] extends object
    ? GetPaths3<T[K], Join<K, P>>
    : Join<K, P>;
}[keyof T] | (P extends '' ? never : P);

type GetPaths3<T, P = ''> = {
  [K in keyof T]: T[K] extends object
    ? GetPaths4<T[K], Join<K, P>>
    : Join<K, P>;
}[keyof T] | (P extends '' ? never : P);

type GetPaths4<T, P = ''> = {
  [K in keyof T]: T[K] extends object
    ? GetPaths5<T[K], Join<K, P>>
    : Join<K, P>;
}[keyof T]  | (P extends '' ? never : P)

type GetPaths5<T, P = ''> = {
  [K in keyof T]: T[K] extends object
    ? GetPaths6<T[K], Join<K, P>>
    : Join<K, P>;
}[keyof T] | (P extends '' ? never : P);

type GetPaths6<T, P = ''> = {
  [K in keyof T]: T[K] extends object
    ? GetPaths7<T[K], Join<K, P>>
    : Join<K, P>;
}[keyof T] | (P extends '' ? never : P);

type GetPaths7<T, P = ''> = {
  [K in keyof T]: T[K] extends object
    ? GetPaths8<T[K], Join<K, P>>
    : Join<K, P>;
}[keyof T] | (P extends '' ? never : P);

type GetPaths8<T, P = ''> = {
  [K in keyof T]: T[K] extends object
    ? GetPaths9<T[K], Join<K, P>>
    : Join<K, P>;
}[keyof T] | (P extends '' ? never : P);

type GetPaths9<T, P = ''> = {
  [K in keyof T]: T[K] extends object
    ? GetPaths10
    : Join<K, P>;
}[keyof T] | (P extends '' ? never : P);

type GetPaths10 = '';

// 提取属性路径类型
export type Paths<T> = GetPaths<T> & string
