import { isArray } from '../is';
import { cloneDeep } from '../fun';
import { parsePath } from '../object';

/**
 * 解析字符串为对象
 * @param s 字符串，如果值为undefined或null，则直接返回默认值
 * @param opt 配置选项
 * @param opt.defaultValue 默认值
 * @param opt.parseFun 是否解析方法字符串
 * @param opt.funPrefix 方法字符串前缀
 * @param opt.rootStr 根对象的字符串表示
 * @return T
 */
export type parseOption<T> = {
  defaultValue?: T,
  parseFun?: boolean,
  funPrefix?: string,
  rootStr?: string
}

const defaultParseOption:parseOption<unknown> = {
  parseFun: false,
  funPrefix: "#YFUN: ",
  rootStr: "#root"
}

export function parse<T>(s: string | null | undefined, opt?: parseOption<T>) {
  //兼容1.1.7之前第二个参数为默认值的写法
    if (opt !== undefined) {
      const paramKeys: (keyof parseOption<T>)[]= ["defaultValue", "parseFun", "funPrefix", "rootStr"];
      const keys = Object.keys(opt);
      //如果参数数量大于配置对象数量或 传入的配置对象的属性中有不属于配置对象的属性，  或者传入的对象是一个{}, 则视为默认值
      if (keys.length > paramKeys.length || keys.some((k) => !paramKeys.includes(k as keyof parseOption<T>)) || keys.length === 0) {
        opt = {
          defaultValue: cloneDeep(opt) as T
        }
      }
    }


  const {defaultValue, parseFun, rootStr, funPrefix} = {
    ...defaultParseOption,
    ...opt
  }

  if (s === null || s === undefined) {
    return defaultValue;
  }

  let result = defaultValue;
  try {
    result = JSON.parse(s);
  } catch (e) {
    if (defaultValue === undefined) {
      console.warn("解析JSON对象失败，并未提供默认值。 值：\n" + s)
      console.warn("parse JSON object error. no default value, value is: \n" + s);
    }
  }

  /**
   * 解析如#root, #root.first, #funfunction () {}
   */
  function _parse(obj: unknown) {
     if (typeof obj === "string") {
       //如果是字符串并且已rootStr开头，则认为是一个循环引用对象
       if (obj.startsWith(rootStr!)) {
          const path = obj.substring(rootStr!.length + 1);
          // originObj则为字符串对应的真正的对象;
         return parsePath(result as object, path);
       }

       if (parseFun && obj.startsWith(funPrefix!)) {
         const funStr = obj.substring(funPrefix!.length);
         return  eval(`r = ${funStr}`);
       }
     }

     //如果是对象或数组
     if (typeof obj === "object" && obj !== null) {
       for (const key in obj) {
         (<Record<string, unknown>>obj)[key] = _parse((<Record<string, unknown>>obj)[key])
       }
     }
     return obj;
  }

  return _parse(result);
}

type stringifyOption = {
  fun?: boolean,
  funPrefix?: string,
  rootStr?: string
}


const defaultStringifyOption:stringifyOption = {
  fun: false,
  funPrefix: defaultParseOption.funPrefix,
  rootStr: defaultParseOption.rootStr
}

/**
 * 字符串化一个对象
 * @param obj 需要被字符串化的对象
 * @param opt 配置选项
 * @param opt.fun 是否字符串化函数，默认关闭
 * @param opt.funPrefix 如果需要字符串化函数，则会返回前缀加函数体
 * @param opt.rootStr 在面对嵌套对象时，表示根对象的名称，默认$root
 */
export function stringify<T>(obj: T, opt: stringifyOption = {}): string {
  const option: stringifyOption = {
    ...defaultStringifyOption,
    ...opt
  }

  /**
   * 用于存储已经序列化后的obj
   */
  const objMap = new WeakMap();

  function _stringify(obj: unknown, name:string) {
    if (typeof obj === "function" && option.fun) {
      return JSON.stringify(`${option.funPrefix}${obj.toString()}`);
    }

    // 如果不是对象，则直接使用stringify
    if (typeof obj !== "object" || obj === null) {
      return JSON.stringify(obj);
    }

    //如果之前已经尝试序列化这个对象，则用深属性名代替，如$root.b.x.1.2
    if (objMap.has(obj)) {
      return `"${objMap.get(obj)}"`;
    }
    //如果集合里没有这个对象的引用，则将其放入
    objMap.set(obj, name);

    let result = "";

    try {
      result = JSON.stringify(obj)
    } catch (e) {
      //如果是嵌套错误
      if ((<Error>e).message.startsWith("Converting circular structure to JSON")) {
        for (const key in obj) {
          if (!obj.hasOwnProperty(key)) {
            continue;
          }

          const v = (<Record<string, unknown>>obj)[key];
          // 如果是数组，则内容是v,v，如果是对象，则内容是key:v,key:v,
          result += isArray(obj) ? `${_stringify(v, name + "." + key)},` : `"${key}":${_stringify(v, name + "." + key)},`;
        }
        //删掉最后面的,
        result = Object.keys(obj).length > 0 ? result.substring(0, result.length -1) : result;
        // 如果是数组则[]包围，对象用{}包围
        isArray(obj) ? result = `[${result}]` : result = `{${result}}`
      }
    }
    return  result;
  }

return  _stringify(obj, option.rootStr!);
}
