import { isFunction, isString } from '../is';

declare global {
  interface Array<T> {
    readonly last: T | undefined;
    readonly first: T | undefined;
    readonly isEmpty: boolean;
    readonly isNotEmpty: boolean;
    distinct(compare?: CompareFn<T> | keyof T): T[];
  }
}

export function installArray() {
  // 如果已经安装过了，直接返回
  if (Array.prototype.distinct !== undefined) {
    console.warn("无需重复install");
    return;
  }
  Object.defineProperty(Array.prototype, 'distinct', {
    value: function<T>(this: T[], compare: keyof T | CompareFn<T>) {
      return distinct(this, compare);
    },
    enumerable: false,
    writable: false,
    configurable: false,
  })

  Object.defineProperty(Array.prototype, 'last', {
    get: function<T> (this: T[]) {
      if (this.length === 0) {
        return undefined;
      }
      return this[this.length - 1];
    },
    enumerable: false,
  })

  Object.defineProperty(Array.prototype, 'first', {
    get: function<T> (this: T[]) {
      if (this.length === 0) {
        return undefined;
      }
      return this[0];
    },
    enumerable: false,
  })

  Object.defineProperty(Array.prototype, 'isEmpty', {
    get: function<T> (this: T[]) {
      return this.length === 0;
    },
    enumerable: false,
  })

  Object.defineProperty(Array.prototype, 'isNotEmpty', {
    get: function<T> (this: T[]) {
      return this.length !== 0;
    },
    enumerable: false,
  })
}

type CompareFn<T> = (a: T, b: T) => boolean;


/**
 * 数组去重
 *
 * @return 新数组
 * @param target - 目标数组
 * @param compareFn - 比较函数 | 字段名
 * @description 不改变原数组,返回新数组
 */
export function distinct<T>(target: Iterable<T>, compareFn?:CompareFn<T> | keyof T): T[] {
  const result: T[] = [];
  let fun: CompareFn<T>;
  if (isString(compareFn)) {
    fun = (a, b) => a[compareFn] === b[compareFn];
  } else if (isFunction(compareFn)) {
    fun = compareFn as CompareFn<T>;
  } else if (compareFn === undefined) {
    fun = (a, b) => a === b;
  } else  {
    throw new Error('参数类型错误');
  }

  target: for (const item of target) {
    for (const resultItem of result) {
      if (fun(item, resultItem)) {
        continue target;
      }
    }
    result.push(item);
  }
  return result;
}

installArray();


