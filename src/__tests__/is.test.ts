import { isSame } from '../is';

test("parseUrlParams", () => {
  expect(isSame({name: "123"}, {name: "123"})).toEqual(true);
  expect(isSame({name: "123"}, {name: "1234"})).toEqual(false);
  expect(isSame({name: {age: 123}}, {name: {age: 123}})).toEqual(true);
  expect(isSame({name: {age: 123}}, {name: {age: 1234}})).toEqual(false);
  expect(isSame([{name: 123, age: 456}], [{name: 123, age: 456}])).toEqual(true);
  expect(isSame([{name: 123, age: 456}], [{name: 123, age: 4567}])).toEqual(false);
})