import { distinct } from '../array';

test("distinct", () => {
  const arr = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
  expect(distinct(arr)).toEqual([1, 2, 3, 4, 5, 6, 7, 8, 9, 10]);

  const arr2 = [{ value: 1}, { value: 2}, { value: 3}, { value: 4}, { value: 5}, {value: 1}]
  expect(distinct(arr2, 'value')).toEqual([{ value: 1}, { value: 2}, { value: 3}, { value: 4}, { value: 5}])

  const arr3 = [[{ value: 1}], [{ value: 2}], [{ value: 3}], [{ value: 1}]]
  expect(distinct(arr3, (a,b) => a[0].value === b[0].value))

  const arr4 = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
  expect(arr4.distinct()).toEqual([1, 2, 3, 4, 5, 6, 7, 8, 9, 10]);

  expect([].isEmpty).toEqual(true)
  expect([1,2,3].isEmpty).toEqual(false)
  expect([].isNotEmpty).toEqual(false)
  expect([1,2,3].isNotEmpty).toEqual(true)
})
