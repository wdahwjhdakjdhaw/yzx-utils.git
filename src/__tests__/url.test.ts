import { joinUrl, joinUrlParams, parseUrlParams } from '../url';
import { installArray } from '../array';

test('joinUrlParam', () => {
  expect(joinUrlParams("https://www.xxx.com", {})).toBe("https://www.xxx.com");
  expect(joinUrlParams("https://www.xxx.com", { a: 1 })).toBe("https://www.xxx.com?a=1");
  expect(joinUrlParams("https://www.xxx.com", { a: 1, b: 2 })).toBe("https://www.xxx.com?a=1&b=2");
  expect(joinUrlParams("https://www.xxx.com", { a: 1, b: 2, c: {} })).toBe("https://www.xxx.com?a=1&b=2&c=%7B%7D");
  expect(joinUrlParams("https://www.xxx.com", {a: []})).toBe("https://www.xxx.com?a=%5B%5D")
});

test("parseUrlParams", () => {
  const url = "https://www.xxx.com?a=1&b=2&c={}";
  expect(parseUrlParams(url)).toEqual({ a: "1", b: "2", c: "{}" });
  expect(parseUrlParams("https://www.xxx.com?name=哈哈")).toEqual({name: "哈哈"})
})

test("joinUrl", () => {
  const url = "https://www.xxx.com";
  installArray();
  expect(joinUrl(url, "/a", "/c", "w")).toEqual("https://www.xxx.com/a/c/w")
  expect(joinUrl(url, "/a", "/c", "w", "../")).toEqual("https://www.xxx.com/a/c")
  expect(joinUrl(url, "/a", "/c", "w", "", "/", "/")).toEqual("https://www.xxx.com/a/c/w/")
  expect(joinUrl(url + "/", "/a", "/c", "w", "")).toEqual("https://www.xxx.com/a/c/w")
  expect(joinUrl(url, "/a", "/c", "w", {a:1, c: 2})).toEqual("https://www.xxx.com/a/c/w?a=1&c=2")
  expect(joinUrl(url, "/a", "/c", "w/",{a:1, c: 2})).toEqual("https://www.xxx.com/a/c/w/?a=1&c=2")
})
