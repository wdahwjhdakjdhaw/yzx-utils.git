import { cloneDeep } from '../fun';

test("cloneDeep", () => {
  const obj: any = {name: "test", age: 18, children: {name: "test1", age: 19}};
  obj.children.father = obj;
  const cloneObj = cloneDeep(obj);
  expect(cloneObj).not.toBe(obj);
})