import { toNumber } from '../string';

test("string", () => {
  expect(toNumber("10")).toBe(10);
  expect(toNumber("10.1")).toBe(10.1);
  expect(toNumber("10.1.1")).toBeNaN();
  expect(toNumber("-421.5")).toBe(-421.5);
  expect("12.5".toNumber()).toBe(12.5);
  expect("0258".toNumber({radix: 16, signed: true})).toBe(600)
  expect("FF".toNumber({radix: 16, signed: true})).toBe(-1);
  expect("FE".toNumber({radix: 16, signed: true})).toBe(-2);
  expect("10101011".toNumber({radix: 2, signed: true})).toBe(-85);
  expect("10101011".toNumber({radix: 2, signed: false})).toBe(171);
})
