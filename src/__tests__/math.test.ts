import { uuid } from '../math';


test('uuid', () => {
  expect(uuid().length).toBe(36);
  expect(uuid(32).length).toBe(32);
  expect(uuid() === uuid()).toBe(false);
});
