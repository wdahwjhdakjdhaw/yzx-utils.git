import { parse, parseOption, stringify } from '../json';
test("parseUrlParams", () => {
  expect(parse("{}")).toEqual({});
  expect(parse("undefined", {defaultValue: 100})).toEqual(100);
  expect(parse(null, {defaultValue: []})).toEqual([]);
  expect(parse(undefined, {defaultValue: {}})).toEqual({});

  //
  const nest: Record<string, unknown> = {};
  nest.first = nest;

  const r = parse(stringify(nest));
  expect(r.first === r).toBe(true);

  //兼容测试
  expect(parse(undefined, {})).toEqual({});
  expect(parse(undefined, {h: 1} as unknown as parseOption<unknown>)).toEqual({h: 1});
  expect(parse(undefined, []  as unknown as parseOption<unknown>)).toEqual([]);
  expect(parse(undefined, 1  as unknown as parseOption<unknown>)).toEqual(1);
})

test("stringify", () => {
  const empty = {};
  expect(stringify(empty)).toEqual(JSON.stringify({}));

  const primitive = 0;
  expect(stringify(primitive)).toEqual(JSON.stringify(primitive));

  const nest: Record<string, any> = {};
  const first:Record<string, unknown> = {
    nest: nest
  }

  nest.first = first;
  expect(stringify(nest, {rootStr: "$root"})).toEqual(`{"first":{"nest":"$root"}}`);

  first.self = first;
  expect(stringify(nest, {rootStr: "#root"})).toEqual(`{"first":{"nest":"#root","self":"#root.first"}}`)

  first.arr = [first];
  expect(stringify(nest, {rootStr: "#root"})).toEqual(`{"first":{"nest":"#root","self":"#root.first","arr":["#root.first"]}}`);

  const fun = function(){console.log("i am fun");};

  nest.first.fun = fun;

  expect(stringify(nest, {
    fun: true,
    funPrefix: "#fun",
    rootStr: "$root"
  })).toEqual(`{"first":{"nest":"$root","self":"$root.first","arr":["$root.first"],"fun":"#funfunction () { console.log(\\"i am fun\\"); }"}}`)


  const arrNest: unknown[] = [];
  const arrFirst: unknown[] = [];

  arrFirst[0] = arrNest;
  arrNest[0] = arrFirst;

  expect(stringify(arrNest, {
    rootStr: "$root"
  })).toEqual(`[["$root"]]`);

  const arrSecond = {
    root: arrFirst
  };

  arrNest[1] = arrSecond;

  expect(stringify(arrNest, {rootStr: "$root"})).toEqual(`[["$root"],{"root":"$root.0"}]`);





})