import { formatDate, getDurStr, isToday, subDate } from '../date';

test('date', () => {
  const dateStringDefault = formatDate(new Date(1970, 0, 1, 0, 0, 0, 0));
  const dateStringYYYYDDMM = formatDate(new Date(1970, 0, 1, 0, 0, 0, 0), 'YYYY-DD-MM');
  expect(dateStringDefault).toBe('1970-01-01 00:00:00');
  expect(dateStringYYYYDDMM).toBe('1970-01-01');
});

test("subDate", () => {
  expect(subDate(new Date())).toBe('刚刚');
  expect(subDate(new Date('2021-01-01'), new Date('2021-01-02'))).toBe('1天前');
  expect(subDate(new Date('2021-01-01'), new Date('2021-01-01'))).toBe('刚刚');
  expect(subDate(new Date('2021-01-01 00:00:00'), new Date('2021-01-01 00:00:01'))).toBe('1秒前');
  expect(subDate(new Date('2021-01-01 00:00:00'), new Date('2021-01-01 00:01:00'))).toBe('1分钟前');
  expect(subDate(new Date('2021-01-01 00:00:00'), new Date('2021-01-01 01:00:00'))).toBe('1小时前');
  expect(subDate(new Date('2021-01-01'), new Date('2021-01-08'))).toBe('1周前');
  expect(subDate(new Date('2021-01-01'), new Date('2021-02-01'))).toBe('1个月前');
  expect(subDate(new Date('2021-01-01'), new Date('2022-01-01'))).toBe('1年前');
  expect(subDate(new Date('2021-01-01'), new Date('2023-01-01'))).toBe('2年前');
});

test("isToday", () => {
  expect(isToday(new Date())).toBeTruthy();
  expect(isToday(new Date('2021-01-01'))).toBeFalsy();
})

test("getDurStr", () => {
  expect(getDurStr(1200)).toBe('1秒')
  expect(getDurStr(120, 'second')).toBe('2分钟')
  expect(getDurStr(23, 'hour')).toBe('23小时')
  expect(getDurStr(23.1, 'hour')).toEqual('23小时6分钟')
  expect(getDurStr(1440, 'minute')).toBe('1天')
})
