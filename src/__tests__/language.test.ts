import { i18n, Locale } from '../language';

test("language", () => {
  i18n.setLanguage(Locale.zh_CN);
  expect(i18n.t("$Y.date.mill_second")).toBe("毫秒");
  expect(i18n.t("$Y.date.second")).toBe("秒");
  i18n.setLanguage(Locale.en_US);
  expect(i18n.t("$Y.date.mill_second")).toBe("ms");
  expect(i18n.t("$Y.date.second")).toBe("s");
  //重置语言避免影响其他测试
  i18n.setLanguage(Locale.zh_CN);
})
