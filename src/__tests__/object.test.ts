import { hasAttribute, installObject, parsePath } from '../object';

test("object", () => {

  expect(parsePath({ a: { b: 1 } }, "a.b")).toBe(1)
  expect(parsePath({}, 'a.b.c')).toBe(undefined);
  expect(parsePath({ a: { b: { c: 1 } } }, 'a.b.c')).toBe(1);
  expect(parsePath({ a: { b: { c: 1 } } }, 'a.b.c.d')).toBe(undefined);
  expect(parsePath({ a: { b: { c: 1 } } }, 'a.b.c.d.e')).toBe(undefined);
  expect(hasAttribute({ a: { b: { c: 1 } } }, 'a.b.c')).toBe(true)
  expect(hasAttribute({ a: { b: { c: 1 } } }, 'a.b.c.d')).toBe(false)
  expect(hasAttribute({ a: { b: { c: 1 } } }, 'a.b.c.d.e')).toBe(false)
  expect(hasAttribute({ a: { b: { c: 1 } } }, 'a.b')).toBe(true)
  installObject();
  expect({ a: { b: { c: 1 } } }.getAttr('a.b.c')).toBe(1)
  expect({ a: { b: { c: 1 } } }.hasAttr('a.b.c')).toBe(true)
  expect({ a: { b: { c: 1 } } }.hasAttr('a.b.c.d')).toBe(false)
  expect({ a: { b: { c: 1 } } }.hasAttr('a.b')).toBe(true)
  expect({ a: { b: { c: 1 } } }.hasAttr('a.b.c.d.e')).toBe(false)
  expect({ a: { b: { c: 1 } } }.hasAttr('a.b.c.d')).toBe(false)
  expect({ a: { b: { c: 1 } } }.hasAttr('a.b.c.d.e')).toBe(false)
})
