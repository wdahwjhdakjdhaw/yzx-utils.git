import { stringify } from '../json';
import { Paths } from '../type';

declare global {
  interface Object {
    hasAttr:<T>(this: T ,path: Paths<T> | string) => boolean;
    getAttr: <T,R>(this: T, path: Paths<T> | string) => R | undefined;
  }
}

/**
 * @deprecated 废弃，使用getAttribute代替
 * 从一个对象中获取"xxx.xxx.xxx"的值, 如果对象不存在该属性，则返回undefined
 */
export function parsePath<O extends object, R>(obj: O, path: Paths<O> | string): R | undefined {
  if (typeof path !== "string") {
    throw Error("parsePath路径参数必须为字符串, path为" + stringify(path))
  }
  // 如果path是空字符串或者其他假值
  if (!path) {
    return obj as unknown as R;
  }

  const attributes = path.split(".") as (keyof O)[];
  let result:O = obj;
  for (const attribute of attributes) {
    if (!result) {
      return undefined;
    }
    result = result[attribute] as O;
  }

  return result as unknown as R;
}

export function getAttribute<O extends object, R>(obj: O, path: Paths<O> | string): R | undefined {
  return parsePath<O, R>(obj, path);
}

/**
 * 判断obj对象中是否有指定的字符串属性
 * @param obj 对象
 * @param path 属性名
 * @returns 是否存在 true有， false没有
 */
export function hasAttribute<O extends object>(obj: O, path: Paths<O> | string): boolean {
  if (typeof path !== "string") {
    throw Error("hasAttribute路径参数必须为字符串, path为" + stringify(path))
  }
  // 如果path是空字符串或者其他假值
  if (!path) {
    return false;
  }

  const attributes = path.split(".") as (keyof object)[];
  let result: object = obj;
  for (const attribute of attributes) {
    if (result.hasOwnProperty(attribute)) {
      result = result[attribute] as object;
    } else {
      return false;
    }
  }
  return true;
}

export function installObject() {
  if (typeof Object.prototype.hasAttr === 'function') {
    console.warn('忽略重复install')
    return
  }
  Object.defineProperty(Object.prototype, 'hasAttr', {
    value: function(path: string) {
      return hasAttribute(this, path);
    },
    configurable: false,
    writable: false,
    enumerable: false,
  })

  Object.defineProperty(Object.prototype, 'getAttr', {
    value: function<R>(path: string) {
      return getAttribute<object, R>(this, path);
    },
    configurable: false,
    writable: false,
    enumerable: false,
  })
}

installObject();
