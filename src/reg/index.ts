const phoneReg = /^(13[0-9]|14[01456879]|15[0-35-9]|16[2567]|17[0-8]|18[0-9]|19[0-35-9])\d{8}$/
const emailReg = /^[a-zA-Z0-9_-]+@[a-zA-Z0-9_-]+(\.[a-zA-Z0-9_-]+)+$/



/**
 * 验证一个字符串是否符合手机号规则
 * @param phone 手机号
 * @return 符合返回真，不符合返回假
 */
export function testPhone(phone: string) {
  return phoneReg.test(phone)
}

/**
 * 验证一个字符串是否是邮箱
 * @param email 邮箱字符串
 * @return 符合返回真，不符合返回假
 */
export function testEmail(email:string) {
  return  emailReg.test(email)
}