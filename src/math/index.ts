/**
 *  返回一个随机数, 可以指定小数的位数
 *  @param {Array} range 范围 [min, max], 默认 [0, 10000]
 *  @param {Number} float 小数位数
 *  @return {Number}
 */
export function getRandomNumber(range: [number, number] = [0, 10000], float: number = 0): number {
  let number = Math.random();
  if (range) {
    number = number * (range[1] - range[0]) + range[0];
  }
  return Number(number.toFixed(float));
}

/**
 * 返回一个随机数数组
 * @param count 数组长度
 * @param range 范围 [min, max], 默认 [0, 10000]
 * @param float 小数位数
 */
export function getRandomNumbers(count: number, range: [number, number] = [0, 10000], float = 0) {
  const numbers = [];
  for (let i = 0; i < count; i++) {
    numbers.push(getRandomNumber(range, float));
  }
  return numbers;
}

/**
 * 需要生成几个汉字
 * @param count
 */
export function getRandomChinese(count: number) {
  let result = "";
  function _getRandomChineseWord() {
    let _rsl = '';
    const _randomUniCode = Math.floor(Math.random() * (40870 - 19968) + 19968).toString(16);
    _rsl = eval( '"\\u' + _randomUniCode + '"');
    return _rsl;
  }

  for (let i = 0; i < count; i++) {
    result += _getRandomChineseWord();
  }

  return result;
}





export const UUID_NUMBER_SET = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9'] as const;
export const UUID_UPPERCASE_SET = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M',
  'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'] as const;
export const UUID_LOWERCASE_SET = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l',
  'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z'] as const;
export const UUID_DEFAULT_SET = [...UUID_NUMBER_SET, ...UUID_LOWERCASE_SET, ...UUID_UPPERCASE_SET] as const;

/**
 * 返回一个uuid
 * @return { String }
 */
export function uuid(): string;
/**
 * 返回一个uuid
 * @param len 长度
 */
export function uuid(len: number): string;
/**
 * 返回一个uuid
 * @param len 长度
 * @param chars 字符集
 */
export function uuid(len: number, chars: string[]): string;

/**
 * 返回一个uuid
 * @return { String }
 */
export function uuid(len: number = 36, chars:string[] | typeof UUID_UPPERCASE_SET  | typeof  UUID_LOWERCASE_SET | typeof UUID_NUMBER_SET | typeof  UUID_DEFAULT_SET = UUID_DEFAULT_SET): string {
  const uuid = [];
  for (let i = 0; i < len; i++) {
    // 0 | 小数 = 取整
    uuid[i] = chars[0 | (Math.random() * 62)];
  }
  return uuid.join('')
}


