/**
 * [undefined,null,false] => false
 * 排除了0,空字符串为假的情况
 * @param value
 */
export function isTrue(value: unknown): value is true {
  return value !== undefined && value !== null && value !== false;
}

/**
 * 确认一个变量是否为对象
 * @param obj
 * @return boolean 为对象时返回true
 */
export function isObject(obj: unknown): obj is object {
  return Object.prototype.toString.call(obj) === '[object Object]';
}

export function isArray(arr: unknown): arr is unknown[] {
  return Array.isArray(arr);
}

export function isString(str: unknown): str is string {
  return typeof str ==='string';
}

export function isNumber(num: unknown): num is number {
  return typeof num === 'number';
}

export function isBoolean(bool: unknown): bool is boolean {
  return typeof bool === 'boolean';
}

export function isSymbol(sym: unknown): sym is symbol {
  return typeof sym ==='symbol';
}

export function isBigInt(big: unknown): big is bigint {
  return typeof big === 'bigint';
}

/**
 * 是否为undefined
 * @param value
 */
export function isUndefined(value: unknown): value is undefined {
  return value === undefined;
}

/**
 * 是否为null
 * @param value
 */
export function isNull(value: unknown): value is null {
  return value === null;
}

/**
 * 返回是否为空对象，如果不是对象也返回true
 * @param value
 */
export function isNullObject(value: unknown): value is Record<string, never> {
  return isObject(value) && Object.keys(value).length === 0;
}


export function isPrimitive(value: unknown): value is string | number | boolean | symbol | bigint | undefined | null {
  return (
    typeof value === 'string' ||
    typeof value === 'number' ||
    typeof value === 'boolean' ||
    typeof value === 'symbol' ||
    typeof value === 'bigint' ||
    value === undefined ||
    value === null
  );
}

export function isFunction(fn: unknown): fn is (...args: unknown[]) => unknown {
  return typeof fn === 'function';
}

/**
 * 返回两个对象之间是否值相等，比如{a: 1} {a: 1}之间值相等，不比较地址值
 */
export function isSame(obj1: unknown, obj2: unknown) {
  if (obj1 === obj2) {
    return true;
  }

  //  如果obj1和2之间有一个不是对象，并且不是数组，则可以返回false, 因为前面已经做过===比较了
  if ((!isObject(obj1) || !isObject(obj2)) && !(isArray(obj1) && isArray(obj2))) {
    return false;
  }


  for (const key in obj1) {
    if (obj1.hasOwnProperty(key)) {
      if (!obj2.hasOwnProperty(key)) {
        return false;
      }

      // eslint-disable-next-line @typescript-eslint/ban-ts-comment
      // @ts-expect-error
      if(!isSame(obj1[key], obj2[key])) {
        return false;
      }
    }
  }
  return true;

}
