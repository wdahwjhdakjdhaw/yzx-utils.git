import zh_CN from './zh_CN.json'
import en_US from './en_US.json'
import { Paths } from '../type';
import { YObject } from '../index';

export enum Locale {
  zh_CN = 'zh_CN',
  en_US = 'en_US',
  zh_TW = 'zh_TW',
}




export class I18n<T extends Record<string, string | object>> {
  public locale: Locale | string = Locale.zh_CN;
  public messages: Record<string, T> = {};

  public t(key: Paths<T>, locale: Locale | string = this.locale): string {
    if (!this.messages[locale]) {
      console.warn(`[i18n] 语言 ${locale} 未找到, messages为`, this.messages);
    }


    if (!YObject.hasAttribute(this.messages[locale], key)) {
      console.warn(`[i18n] key ${key} 在语言 ${locale} 中未找到, messages为`, this.messages);
    }

    return this.messages[locale]?.getAttr(key) as string;
  }

  public add(messages: Record<string, T>) {
    for (const messagesKey in messages) {
      if (this.messages[messagesKey]) {
        Object.assign(this.messages[messagesKey], messages[messagesKey]);
      } else {
        this.messages[messagesKey] = messages[messagesKey];
      }
    }
  }

  public setLanguage(locale: Locale | string) {
    if (!this.messages[locale]) {
      console.warn(`[i18n] 设置语言为 ${locale}, 未在语言包中找到对应信息, 建议先add语言包后setLanguage, 当前messages为`, this.messages);
    }
    this.locale = locale;
  }

  constructor(opt: Partial<I18n<T>>) {
    Object.assign(this, opt);
  }
}

export const i18n = new I18n({ messages: { zh_CN, en_US }, locale: Locale.zh_CN })
