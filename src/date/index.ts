import { i18n } from '../language';

export const TIME_UNIT = {
  YEAR: 'year',
  MONTH: 'month',
  DAY: 'day',
  HOUR: 'hour',
  MINUTE: 'minute',
  SECOND: 'second',
  MILLISECOND: 'millisecond',
} as const;


/**
 * 传入时间戳，返回格式化后的时间
 * @param time 时间戳
 * @param format 格式化字符串 {YYYY-MM-DD hh:mm:ss}
 */
export function formatDate(time: string | number | Date = new Date(), format = 'YYYY-MM-DD hh:mm:ss', fillZero: boolean = true): string {
  if (time === null || time === '') {
    return '';
  }
  const date = new Date(time);
  const year = date.getFullYear(),
    month = date.getMonth() + 1, //月份是从0开始的
    day = date.getDate(),
    hour = date.getHours(),
    min = date.getMinutes(),
    sec = date.getSeconds();
  const preArr = ['00', '01', '02', '03', '04', '05', '06', '07', '08', '09'];
  if (fillZero) {
    return format
      .replace(/YYYY/gi, year.toString())
      .replace(/MM/g, (preArr[month] || month).toString())
      .replace(/DD/gi, (preArr[day] || day).toString())
      .replace(/hh/gi, (preArr[hour] || hour).toString())
      .replace(/mm/g, (preArr[min] || min).toString())
      .replace(/ss/gi, (preArr[sec] || sec).toString());
  }

  return format
    .replace(/YYYY/gi, year.toString())
    .replace(/MM/g, month.toString())
    .replace(/DD/gi, day.toString())
    .replace(/hh/gi, hour.toString())
    .replace(/mm/g, min.toString())
    .replace(/ss/gi, sec.toString());
}


/**
 * @param {number | Date | string } date 时间，需要小于当前日期或compareDate;
 * @param {number | Date} compareDate 比较时间，默认为当前时间
 * @description 传入时间戳或者时间对象，返回距离当前时间的时间差,返回格式为：几分钟前，几小时前，几天前, 几月前, 几年前
 */
export function subDate(date: number | Date | string, compareDate: number | Date = new Date()) {
  compareDate = new Date(compareDate);
  const now = compareDate.getTime();
  const time = new Date(date).getTime();
  const diffValue = now - time;
  if (diffValue < 0) {
    throw new Error(`错误的时间参数, ${date}不能大于${compareDate}`);
  }
  const minute = 1000 * 60;
  const hour = minute * 60;
  const day = hour * 24;
  const month = day * 30;
  const year = month * 12;
  const second = 1000;

  const _year = diffValue / year;
  const _month = diffValue / month;
  const _week = diffValue / (7 * day);
  const _day = diffValue / day;
  const _hour = diffValue / hour;
  const _min = diffValue / minute;
  const _sec = diffValue / second;

  if (_year >= 1) {
    return parseInt(String(_year)) + i18n.t("$Y.date.years_ago")
  } else if (_month >= 1) {
    return parseInt(String(_month)) + i18n.t("$Y.date.months_ago");
  } else if (_week >= 1) {
    return parseInt(String(_week)) + i18n.t("$Y.date.weeks_ago");
  } else if (_day >= 1) {
    return parseInt(String(_day)) + i18n.t("$Y.date.days_ago");
  } else if (_hour >= 1) {
    return parseInt(String(_hour)) + i18n.t("$Y.date.hours_ago");
  } else if (_min >= 1) {
    return parseInt(String(_min)) + i18n.t("$Y.date.minutes_ago");
  } else if (_sec >= 1) {
    return parseInt(String(_sec)) + i18n.t("$Y.date.seconds_ago");
  } else {
    return i18n.t("$Y.date.just_now");
  }
}

/**
 * 判断是否是今天
 * @param date 时间戳或date
 * @returns {boolean} true是今天，false不是今天
 */
export function isToday(date: number | Date) {
  return formatDate(date, 'YYYY-MM-DD') === formatDate(new Date(), 'YYYY-MM-DD');
}

/**
 * 获取一段时间的字符串表示，如120秒表示为两分钟, 121秒表示为2分钟1秒, 3600秒表示为1小时
 */
export function getDurStr(dur: number, unit: typeof TIME_UNIT[keyof typeof TIME_UNIT] = 'millisecond', targetUnit: typeof TIME_UNIT[keyof typeof TIME_UNIT] | 'auto' = 'auto') {
  let mill = dur;
  switch (unit) {
    case TIME_UNIT.YEAR:
      mill = dur * 1000 * 60 * 60 * 24 * 365;
      break;
    case TIME_UNIT.MONTH:
      mill = dur * 1000 * 60 * 60 * 24 * 30;
      break;
    case TIME_UNIT.DAY:
      mill = dur * 1000 * 60 * 60 * 24;
      break;
    case TIME_UNIT.HOUR:
      mill = dur * 1000 * 60 * 60;
      break;
    case TIME_UNIT.MINUTE:
      mill = dur * 1000 * 60;
      break;
    case TIME_UNIT.SECOND:
      mill = dur * 1000;
      break;
    case TIME_UNIT.MILLISECOND:
      break;
  }

  switch (targetUnit) {
    case 'year':
      return `${mill / 1000 / 60 / 60 / 24 / 365}${i18n.t("$Y.date.year")}`;
    case 'month':
      return `${mill / 1000 / 60 / 60 / 24 / 30}${i18n.t("$Y.date.month")}`;
    case 'day':
      return `${mill / 1000 / 60 / 60 / 24}${i18n.t("$Y.date.day")}`;
    case 'hour':
      return `${mill / 1000 / 60 / 60}${i18n.t("$Y.date.hour")}`;
    case 'minute':
      return `${mill / 1000 / 60}${i18n.t("$Y.date.minute")}`;
    case 'second':
      return `${mill / 1000}${i18n.t("$Y.date.second")}`;
    case 'millisecond':
      return `${dur}${i18n.t("$Y.date.mill_second")}`;
    case 'auto':
      if (mill < 1000) {
        return `${dur}${i18n.t("$Y.date.mill_second")}`;
      } else if (mill < 1000 * 60) {
        return `${Math.floor(mill / 1000)}${i18n.t("$Y.date.second")}`;
      } else if (mill < 1000 * 60 * 60) {
        return `${Math.floor(mill / 1000 / 60)}${i18n.t("$Y.date.minute")}${Math.floor((mill / 1000) % 60) !== 0 ? Math.floor((mill / 1000) % 60) + i18n.t("$Y.date.second") : '' }`;
      } else if (mill < 1000 * 60 * 60 * 24) {
        return `${Math.floor(mill / 1000 / 60 / 60)}${i18n.t("$Y.date.hour")}${Math.floor((mill / 1000 / 60) % 60) != 0 ? Math.floor((mill / 1000 / 60) % 60) + i18n.t("$Y.date.minute"): ''}${Math.floor((mill / 1000) % 60) !== 0 ? Math.floor((mill / 1000) % 60) + i18n.t("$Y.date.second") : ''}`;
      } else if (mill < 1000 * 60 * 60 * 24 * 30) {
        return `${Math.floor(mill / 1000 / 60 / 60 / 24)}${i18n.t("$Y.date.day")}${Math.floor((mill / 1000 / 60 / 60) % 24) !== 0 ? Math.floor((mill / 1000 / 60 / 60) % 24) + i18n.t("$Y.date.hour") : ''}`;
      } else if (mill < 1000 * 60 * 60 * 24 * 365) {
        return `${Math.floor(mill / 1000 / 60 / 60 / 24 / 30)}${i18n.t("$Y.date.month")}${Math.floor((mill / 1000 / 60 / 60 / 24) % 30)}${i18n.t("$Y.date.day")}`;
      } else {
        return `${Math.floor(mill / 1000 / 60 / 60 / 24 / 365)}${i18n.t("$Y.date.year")}${Math.floor((mill / 1000 / 60 / 60 / 24) % 365)}${i18n.t("$Y.date.day")}`;
      }
  }


}
