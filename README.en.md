# yzxUtils

#### INTRODUCTION
My common tool collection


#### INSTALLATION TUTORIAL

```shell
npm install yzxutils
```

#### INSTRUCTIONS FOR USE

1. ##### Mount to vue
```typescript
   // main.ts
   import { createApp } from 'vue'
   import yzx from 'yzx-utils'
   app.use(yzx)
```

2. ##### Use directly

```typescript
   import { formatDate } from 'yzx-utils'
```

[git address](https://gitee.com/wdahwjhdakjdhaw/yzx-utils.git)