# yzxUtils

#### 介绍
我的常用工具集合


#### 安装教程

```shell
npm install @yzx-utils/core
```

#### 使用说明

1. ##### 挂载到vue
```typescript
   // main.ts
   import { createApp } from 'vue'
   import yzx from 'yzx-utils'
   app.use(yzx)
```
2. ##### 直接使用
   
```typescript
   import { formatDate } from '@yzx-utils/core'
```

   
### i18n使用说明
1. 定义语言包,格式为对象模式，如在src文件夹下新建i18n文件夹，文件夹内创建zh_CN.json和en_US.json
```angular2html
// zh_CN.json
{
  "hello": "你好"
}
```

```angular2html
// en_US.json
{
  "hello": "hello"
}
```
2. 导入语言包
```angular2html
import { Yi18n } from '@yzx-utils/core'
//设置当前语言，这个实例全局唯一
Yi18n.setLang('zh_CN')
//添加语言包，本工具包还内置了部分语言包，会对同种语言包进行合并
Yi18n.add({zh_CN:zh_CN,en_US:en_US});
```

3. 使用
```ts
console.log(Yi18n.t('hello'))
```

[git地址](https://gitee.com/wdahwjhdakjdhaw/yzx-utils.git)




